<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//['middleware' => ['auth']]

Route::middleware([])->prefix('films')->group(function () {
	Route::get('/', function () {
		$title = 'Films';
		return view('welcome', compact('title'));
	});

	Route::get('create', function () {
		$title = 'Films';
		return view('welcome', compact('title'));
	});
});

Route::redirect('/', '/films');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
