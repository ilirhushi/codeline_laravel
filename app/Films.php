<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $id
 */
class Films extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'description', 'release_date', 'rating', 'ticket_price', 'country', 'photo', 'name'
	];

	public function genders( ) {
		return $this->belongsToMany('App\Gender', 'film_genders', 'film_id', 'gender_id');
	}
}
