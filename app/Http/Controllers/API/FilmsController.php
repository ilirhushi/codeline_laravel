<?php

namespace App\Http\Controllers\API;

use App\Comments;
use App\Films;
use App\Gender;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;


class FilmsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'release_date' => 'required|date',
			'country'      => 'required|string',
			'description'  => 'required|string',
			'gender'       => 'required|array',
			'name'         => 'required|string',
			'photo'        => 'required|string',
			'rating'       => 'required|numeric',
			'ticket_price' => 'required|numeric',
		] );

		if ( $validator->fails() ) {
			$response = [
				'data'    => $validator->errors(),
				'status'  => 0,
				'message' => 'All the fields are required!',
				'type'    => 'error'
			];

			return response()->json( $response, 200 );
		}

		$film = new Films;

		$release_date = $request->input( 'release_date' );
		$country      = $request->input( 'country' );
		$description  = $request->input( 'description' );
		$gender       = $request->input( 'gender' );
		$name         = $request->input( 'name' );
		$photo        = $request->input( 'photo' );
		$rating       = $request->input( 'rating' );
		$ticket_price = $request->input( 'ticket_price' );

		$film->name         = $name;
		$film->country      = $country;
		$film->release_date = Carbon::parse($release_date)->format('Y-m-d H:i:s') ;
		$film->description  = $description;
		$film->photo        = $photo;
		$film->rating       = $rating;
		$film->ticket_price = $ticket_price;

		$film->save();
		$film->genders()->sync( $gender );


		$response = [
			'data'    => $film,
			'status'  => 200,
			'message' => 'Film created successfully!',
			'type'    => 'success'
		];

		return response()->json( $response, 200 );
	}

	/**
	 * All genders of the Films
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @author Ilir HUSHI
	 */
	public function allGenders() {
		$genders = Gender::all()->toArray();

		$response = [
			'data'   => $genders,
			'status' => 200
		];

		return response()->json( $response, 200 );
	}

	/**
	 * Show all films - paginated by 1
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 *
	 * @author Ilir HUSHI
	 */
	public function allFilms( Request $request ) {
		$films = Films::paginate(1);
		$films_array = $films->toArray()['data'];

		$films_array = array_map(function($film){
			$film['comments'] = Comments::where('film_id', $film['id'])->orderBy('created_at', 'desc')->get()->toArray();
			$film['comments'] = array_map(function($comment){
				$comment['user_details'] = User::whereId($comment['user_id'])->first()->toArray();
				return $comment;
			}, $film['comments']);
			return $film;
		}, $films_array);

		$totalFilms = Films::all()->count();

		$authUser = Auth::user();

		$response = [
			'data'   => [
				'films' => $films_array,
				'total' => $totalFilms,
				'active_user' => $authUser
			],
			'status' => 200
		];

		return response()->json( $response, 200 );
	}

	/**
	 * Create a new comment on the film
	 *
	 * @param Request $request
	 *
	 * @author Ilir HUSHI
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function createComment( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'comment' => 'required|string',
			'film_id' => 'required|numeric',
			'user_id' => 'required|numeric',
		] );

		if ( $validator->fails() ) {
			$response = [
				'data'    => $validator->errors(),
				'status'  => 0,
				'message' => 'All the fields are required!',
				'type'    => 'error'
			];

			return response()->json( $response, 200 );
		}

		$comment = $request->input('comment');
		$filmId = $request->input('film_id');
		$userId = $request->input('user_id');

		$newComment = new Comments;

		$newComment->comment = $comment;
		$newComment->user_id = $userId;
		$newComment->film_id = $filmId;

		$newComment->save();

		$response = [
			'data'    => [
				'new_comment' => $newComment
			],
			'status'  => 200,
			'message' => 'New comment saved!',
			'type'    => 'success'
		];

		return response()->json($response, 200);
	}
}
