<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_genders', function (Blueprint $table) {

	        $table->integer('film_id')->unsigned()->index();
	        $table->foreign('film_id')->references('id')->on('films')->onDelete('cascade');

	        $table->integer('gender_id')->unsigned()->index();
	        $table->foreign('gender_id')->references('id')->on( 'GenderSeeder' )->onDelete('cascade');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_genders');
    }
}
