<?php

use Database\traits\DisableForeignKeys;
use Database\traits\TruncateTable;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenderSeeder extends Seeder {
//	use DisableForeignKeys, TruncateTable;

	/**
	 * Run the database seed.
	 *
	 * @return void
	 */
	public function run() {
//		$this->disableForeignKeys();
//		$this->truncate( 'genders' );

		$genders = [
			[
				'name' => 'Fiction',
				'description' => '',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'name' => 'Horror',
				'description' => '',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'name' => 'Adventure',
				'description' => '',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'name' => 'Romance',
				'description' => '',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			]
		];

		DB::table( 'genders' )->insert( $genders );

//		$this->enableForeignKeys();
	}
}
