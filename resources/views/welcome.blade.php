<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{env('APP_NAME')}} @if(!empty($title)) - {{$title}}@endif</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>


</body>
</html>

@extends('layouts.app')
<link href="{{ asset('build/css/app.css') }}" rel="stylesheet" type="text/css">

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if(!empty($title))<div class="panel-heading"> {{$title}} </div>@endif
                    <div class="panel-body">
                        <my-app>Loading...</my-app>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript" src="{{ asset('build/js/polyfills.js') }}?time={{ time() }}"></script>
<script type="text/javascript" src="{{ asset('build/js/vendor.js') }}?time={{ time() }}"></script>
<script type="text/javascript" src="{{ asset('build/js/app.js') }}?time={{ time() }}"></script>

