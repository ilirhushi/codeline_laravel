import {Component, Input, OnInit} from '@angular/core';
import {ServerService} from "../../services/server.service";

@Component({
    selector: 'comment-section',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
    @Input() comments: any;

    commentText: string;
    urlComment = '/api/films/comment/create';

    selectedFilm: any = [];

    constructor(private svcServer: ServerService) {
        // Do stuff
        this.selectedFilm = svcServer.selectedFilm;
    }

    ngOnInit() {
        console.log('Comment component');
    }

    postComment(){
        let data = {
            'comment': this.commentText,
            'film_id': this.selectedFilm.id,
            'user_id': this.svcServer.activeUser.id
        };
        if(this.commentText !== ''){
                this.svcServer.post(this.urlComment, data, []).map(data => data.json()).subscribe(data => {
                    console.log('data-', data);
                });
        }

    }

}
