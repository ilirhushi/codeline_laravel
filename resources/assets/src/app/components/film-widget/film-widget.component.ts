import {Component, Input, OnInit} from '@angular/core';
import {ServerService} from "../../services/server.service";

@Component({
    selector: 'film-widget',
    templateUrl: './film-widget.component.html',
    styleUrls: ['./film-widget.component.scss']
})
export class FilmWidgetComponent implements OnInit {

    @Input() film: any;
    svcServer: any;

    constructor( _svcServer: ServerService) {
        this.svcServer = _svcServer;
    }

    ngOnInit() {
        console.log('Film widget');
    }

}
