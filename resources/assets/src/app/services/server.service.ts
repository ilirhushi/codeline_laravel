/**
 * Created by Ilir on 6/7/2017.
 */
import {Injectable} from '@angular/core';
// import { Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Http, Headers} from '@angular/http';
import {HttpClient} from "@angular/common/http";
import {async} from "q";

declare var window;

@Injectable()
export class ServerService {
    selectedFilm: any = [];
    activeUser:any = [];

    constructor(private httpClient: Http, private http: HttpClient) {
    }

    /**
     * Post Data in Object
     *
     * @param {any} url : text, url where the data will be posted
     * @param body : object, data to be posted
     * @param _headers: array of objects, e.g.: [{key: 'Content-Type', value: 'application/x-www-form-urlencoded'}]
     *
     * @return {Observable<Response>} : response body of the fired post
     *
     * @author Ilir HUSHI
     */
    post(url = null, body = null, _headers = []): Observable<any> {
        let headers = new Headers();
        // headers.set('Content-Type', 'application/x-www-form-urlencoded');
        // headers.set('Authorization', 'Bearer ' + access_token);

        /**
         * Set headers from the _headers parameter
         * into the POST header request
         */
        if (_headers.length > 0) {
            _headers.map((header) => {
                headers.set(header.key, header.value);
            });
        }

        return this.httpClient.post(url, body, {headers: headers});
    }


    /**
     * Get Data
     *
     * @param {any} url : text, url where the data will be get
     * @param {Array} _headers : array of objects, e.g.: [{key: 'Content-Type', value: 'application/x-www-form-urlencoded'}]
     *
     * @return {Observable<any>}
     */
    get(url = null, _headers = []): Observable<any> {
        let headers = new Headers();
        // headers.set('Content-Type', 'application/x-www-form-urlencoded');
        // headers.set('Authorization', 'Bearer ' + access_token);

        /**
         * Set headers from the _headers parameter
         * into the GET header request
         */
        if (_headers.length > 0) {
            _headers.map((header) => {
                headers.set(header.key, header.value);
            });
        }

        return this.httpClient.get(url, {headers: headers});
    }

    /**
     * Issue the access token
     * to use the API
     *
     * @return {Observable<any>}
     */
    issueAccessToken(): Observable<any> {
        let data = {
            'grant_type': 'password',
            'client_id': 2,
            'client_secret': 'FLXAWrBUC3d3406Z62221lj6Fq0Kz3VzV5BuH3du',
            'username': 'ilirhushi@gmail.com',
            'password': '111111'
        };

        let url = '/oauth/token';

        return this.http.post(url, data);
    }

}