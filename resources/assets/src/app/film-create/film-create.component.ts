import { Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { ServerService } from '../services/server.service';
import { Router} from '@angular/router';

@Component({
    selector: 'film-create',
    templateUrl: './film-create.component.html',
    styleUrls: ['./film-create.component.scss']
})
export class FilmCreateComponent implements OnInit {

    model = new Movie();

    createUrl = '/api/films/create';
    allGendersUrl = '/api/films/all-genders';
    genders: any = [];

    constructor(private svcService: ServerService, private router: Router) {
        // Do stuff
    }

    ngOnInit() {
        // load all genders of film
        this.getGenders();
        console.log('Films create page');
    }

    getGenders(){
            this.svcService.get(this.allGendersUrl, []).map((data) => data.json()).subscribe((data) => {
                if ( data.status === 200 ) {
                    this.genders = data.data;
                }
            });
    }

    /**
     * submit the form
     * @param {NgForm} myForm
     */
    register (myForm: NgForm) {
        /**
         * if all required fields are filled
         */
        if ( myForm.valid ) {
            console.log('Successful registration');
            let data = this.model;
                this.svcService.post(this.createUrl, data, []).map((data) => data.json()).subscribe((data) => {

                    console.log('data: ', data);
                    if ( data.status === 0 ) {
                        let errors = data.data;
                        if ( errors.hasOwnProperty('rating') ) {
                            this.model.rating = null;
                            alert(errors.rating);
                        }

                        if ( errors.hasOwnProperty('ticket_price') ) {
                            this.model.ticket_price = null;
                            alert(errors.ticket_price);
                        }
                    }

                    if ( data.status === 200 ) {
                        this.router.navigate(['/films']);
                    }
                });

        }
        console.log(myForm);
        console.log('model: ', this.model);
    }

}

export class Movie {
    name: string;
    description: string;
    release_date: any = moment();
    rating: number;
    ticket_price: number;
    country: string;
    gender: any;
    photo: string;
}
