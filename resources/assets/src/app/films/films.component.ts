import {Component, OnInit} from '@angular/core';
import {ServerService} from "../services/server.service";

@Component({
    selector: 'my-films',
    templateUrl: './films.component.html',
    styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {
    p = 1;
    start = 0;
    paginate = 1;
    total = 0;

    filmsUrl = '/api/films/all-films';

    films: any = [];
    selectedWidget: any = [];

    constructor(private svcServer: ServerService) {
        // Do stuff
    }

    ngOnInit() {
        this.getFilms();
        console.log('Films page');
    }

    getFilms() {
        let data = {
            'page': this.p,
            'paginate': this.paginate
        };
            this.svcServer.post(this.filmsUrl, data,[]).map(data => data.json()).subscribe(data => {
                console.log('data:: ', data);
                if(data.status === 200){
                    this.films = data.data.films;
                    this.total = data.data.total;
                    this.svcServer.activeUser = data.data.active_user;
                }
            });

    }

    getPageChange(page: number) {
        this.p = page;
        this.paginate = (this.p * 1) - 1;

        this.getFilms();
    }
}
