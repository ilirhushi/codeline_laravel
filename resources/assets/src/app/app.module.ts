import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { DatePickerModule } from 'angular-io-datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import {HttpClientModule} from '@angular/common/http';


import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { FilmsComponent } from './films/films.component';
import { FilmWidgetComponent } from './components/film-widget/film-widget.component';
import { CommentComponent } from './components/comments/comment.component';
import { FilmDetailsComponent } from './film-details/film-details.component';
import { FilmCreateComponent } from './film-create/film-create.component';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

/**
 * Services
 */
import { ServerService } from './services/server.service';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
      HttpClientModule,
    FormsModule,
      DatePickerModule,
      routing,
      NgxPaginationModule
  ],
  declarations: [
      AppComponent,
      FilmsComponent,
      FilmWidgetComponent,
      CommentComponent,
      FilmDetailsComponent,
      FilmCreateComponent
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
