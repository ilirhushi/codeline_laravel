import {Component, OnDestroy, OnInit} from '@angular/core';
import {ServerService} from "../services/server.service";

@Component({
    selector: 'film-details',
    templateUrl: './film-details.component.html',
    styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit, OnDestroy {
    film: any = [];

    constructor(private svcServer: ServerService) {
        // Do stuff
    }

    ngOnInit() {
        this.film = this.svcServer.selectedFilm;
        console.log('Films details page');
    }

    ngOnDestroy(){
        this.svcServer.selectedFilm = [];
    }

}
