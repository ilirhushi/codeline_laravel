import {RouterModule, Routes} from '@angular/router';

import {FilmsComponent} from './films/films.component';
import {FilmDetailsComponent} from './film-details/film-details.component';
import {FilmCreateComponent} from './film-create/film-create.component';

const routes: Routes = [
    {path: '', component: FilmsComponent},
    {
        // NG Films Routes
        path: 'films', pathMatch: 'prefix', children: [
            {path: '', component: FilmsComponent},
            {path: 'create', component: FilmCreateComponent},
            {path: ':id', component: FilmDetailsComponent}
        ]
    },

];

export const routing = RouterModule.forRoot(routes);
